export interface Users {
    username: string
    password: string
    name: string
    telephone: string
}