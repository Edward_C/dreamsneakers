import * as Knex from "knex";

const ordersTable = "orders"

export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable(ordersTable);
    if (hasTable){
        return knex.schema.alterTable(ordersTable,(table)=>{
            table.integer("quantity").nullable().alter();
            table.string("deliver_place").nullable().alter();
            table.integer("amount").nullable().alter();
            table.string("payment_method").nullable().alter();
            table.integer('fk_users_id').nullable().alter();
        });
    }else{
        return Promise.resolve();
    }
}
        
export async function down(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable(ordersTable);
    if (hasTable){
        return knex.schema.alterTable(ordersTable,(table)=>{
            table.integer("quantity").notNullable().alter();
            table.string("deliver_place").notNullable().alter();
            table.integer("amount").notNullable().alter();
            table.string("payment_method").notNullable().alter();
            table.integer('fk_users_id').notNullable().alter();
        });
    }else{
        return Promise.resolve();
    }
}

