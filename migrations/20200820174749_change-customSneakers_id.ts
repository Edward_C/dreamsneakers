import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable("cart_temp" , (table) => {
        table.renameColumn('fk_customSneakers_id', 'fk_customsneakers_id')
    })    
    await knex.schema.alterTable("orders" , (table) => {
        table.renameColumn('fk_customSneakers_id', 'fk_customsneakers_id')
    })    
    await knex.schema.alterTable("customsneakers" , (table) => {
        table.renameColumn('customSneakers_id', 'customsneakers_id')
    })    
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.table("cart_temp" , (table) => {
        table.renameColumn('fk_customsneakers_id', 'fk_customSneakers_id')
    })  
    await knex.schema.table("orders" , (table) => {
        table.renameColumn('fk_customsneakers_id', 'fk_customSneakers_id')
    })  
    await knex.schema.alterTable("customsneakers" , (table) => {
        table.renameColumn('customsneakers_id', 'customSneakers_id')
    })
}

