import * as Knex from "knex";

const sneakersTable = "sneakers"

export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable(sneakersTable)
    if(hasTable){
        await knex.schema.alterTable(sneakersTable , (table) => {
            table.string("image").nullable().alter();
            table.string("sneakers_name");
            table.string("sneakers_description")
            table.string("price")
        })       
    }else{
        return Promise.resolve();
    }
}


export async function down(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable(sneakersTable)
    if(hasTable){
        await knex.schema.alterTable(sneakersTable , (table) => {
            table.string("image").notNullable().alter();
            table.dropColumn("sneakers_name")
            table.dropColumn("sneakers_description",)          
            table.dropColumn("price")
        })       
    }else{
        return Promise.resolve();
    }
}

