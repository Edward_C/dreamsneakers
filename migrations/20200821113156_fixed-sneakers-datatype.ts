import * as Knex from "knex";

const sneakersTable = "sneakers"

//Need to await knex("sneakers").del() before running this.

export async function up(knex: Knex): Promise<void> {
    console.log(`Need to DELETE FROM orders,cart_temp,sneakers before running this!!!!`)
    const hasTable = await knex.schema.hasTable(sneakersTable)
    if(hasTable){
        await knex.schema.table(sneakersTable , (table) => {
            table.integer("price").alter()
        })       
    }
    return
}


export async function down(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable(sneakersTable)
    if(hasTable){
        await knex.schema.table(sneakersTable , (table) => {
            table.string("price").alter()
        })       
    }
    return
}

