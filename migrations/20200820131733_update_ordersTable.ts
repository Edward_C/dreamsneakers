import * as Knex from "knex";

const orders = "orders"

export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable(orders);
    if(hasTable){
        return knex.schema.table(orders,(table) => {
            table.integer("fk_customSneakers_id");
            table.foreign("fk_customSneakers_id").references("customsneakers.customSneakers_id")
        })
    } else {
        return Promise.resolve();
    }
}


export async function down(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable(orders);
    if(hasTable){
        await knex.schema.table(orders , (table) => {
            table.dropColumn('fk_customSneakers_id');
        })      
    }else{
        return Promise.resolve();
    }
}

