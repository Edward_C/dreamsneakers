import * as Knex from "knex";

const usersTable = "users"
const sneakersTable = "sneakers"

export async function up(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable(usersTable)){
        return;
    } else {
        await knex.schema.createTable(usersTable , (table) => {
            table.increments("users_id")
            table.string("username").notNullable().unique();
            table.string("password").notNullable();
            table.string("role").notNullable();
            table.timestamp("created_at");
            table.timestamp("updated_at");
            table.string("name").notNullable;
            table.integer("telephone");
        })

        await knex.schema.createTable(sneakersTable , (table) => {
            table.increments("sneakers_id")
            table.string("image").notNullable();
            table.string("ranking")
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(usersTable);
    await knex.schema.dropTable(sneakersTable);
}

