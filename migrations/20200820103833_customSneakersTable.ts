import * as Knex from "knex";

const custom = "customsneakers"

export async function up(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable(custom)){
        return;
    } else {
        await knex.schema.createTable(custom , (table) => {
            table.increments("customSneakers_id")
            table.string("sneaker_name").notNullable();
            table.string("model").notNullable();
            table.integer("price").notNullable();
        })
    }
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(custom);
}
