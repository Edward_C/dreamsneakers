import * as Knex from "knex";

const recommendTable = "recommendations"

export async function up(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable(recommendTable)){
        return;
    } else {
        await knex.schema.createTable(recommendTable , (table) => {
            table.increments("rec_id")
            table.string("image").notNullable().unique();
            table.string("tag").notNullable();
            table.integer("fk_users_id").notNullable();
            table.integer("fk_sneakers_id").notNullable();
            table.foreign("fk_users_id").references("users.users_id")
            table.foreign("fk_sneakers_id").references("sneakers.sneakers_id")
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(recommendTable);
}