import * as Knex from "knex";

const cartTemp = "cart_temp"

export async function up(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable(cartTemp)){
        return;
    }else{
        await knex.schema.createTable(cartTemp,(table)=>{
            table.increments("cart_temp_id");
            table.integer('fk_users_id').notNullable();
            table.integer("fk_sneakers_id")
            table.foreign('fk_users_id').references('users.users_id')
            table.foreign("fk_sneakers_id").references("sneakers.sneakers_id")
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(cartTemp);
}

