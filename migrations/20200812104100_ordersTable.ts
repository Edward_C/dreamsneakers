import * as Knex from "knex";

const ordersTable = "orders"

export async function up(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable(ordersTable)){
        return;
    } else {
        await knex.schema.createTable(ordersTable , (table) => {
            table.increments("orders_id");
            table.integer("quantity").notNullable();
            table.string("deliver_place").notNullable();
            table.integer("amount").notNullable();
            table.string("payment_method").notNullable();
            table.string("orders_status")
            table.integer('fk_users_id').notNullable();
            table.integer("fk_sneakers_id")
            table.foreign('fk_users_id').references('users.users_id')
            table.foreign("fk_sneakers_id").references("sneakers.sneakers_id")
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(ordersTable);
}


