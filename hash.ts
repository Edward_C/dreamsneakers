import bcrypt from 'bcryptjs';

const SALT_ROUNDS = 10;

export async function hashPassword(plainPassword:string) {
    const hash = await bcrypt.hash(plainPassword,SALT_ROUNDS);
    // console.log(hash)
    return hash;
};


export async function checkPassword(plainPassword:string,hashPassword:string){
    const match = await bcrypt.compare(plainPassword,hashPassword);
    return match;
}

// hashPassword("1234")
//     .then(console.log);

// checkPassword("admin","$2a$10$r8ROGn2D7jnOu4l6Rj14leHghi9GKbdSa54.dd80.dqJLEruZsWri")
//     .then(console.log);