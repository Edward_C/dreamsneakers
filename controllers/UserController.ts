import express from "express";
import fetch from 'node-fetch'
import { UserService } from "../services/UserService";
import { logger } from "../logger";
import HttpStatus from "http-status-codes";
import { hashPassword, checkPassword } from "../hash";
import { cryptoPassword } from "../crypto";



export class UserController {
    constructor(private userService: UserService) { }
    attach(userRouter: express.Router) {
        userRouter.get('/', this.getCurrentUser)
        userRouter.post('/login', this.postLogin)
        userRouter.post('/', this.post)
        userRouter.get('/logout', this.getLogout)
        userRouter.get('/login/google', this.getLoginGoogle)
    }

    getCurrentUser = async (req: express.Request, res: express.Response) => {
        try {
            if (req.session && req.session.user) {
                // const result = await this.userService.getUser(req.session.user.username)
                // delete result[0].password
                const result = req.session.user
                delete result.password
                res.status(200).json(result)
                return
            }
            res.json({ message: "Not yet login" })
        } catch (e) {
            logger.error(e.message)
            res
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .json({ message: "Fail check login" })
        }
    }

    postLogin = async (req: express.Request, res: express.Response) => {
        try {
            const { username, password } = req.body
            const before = new Date()
            const result = await this.userService.getUser(username)
            const after = new Date()
            console.log(after.getTime() - before.getTime())
            const found = result[0]
            console.log(found)
            if (found && await checkPassword(password, found.password)) {
                if (req.session) {
                    req.session.user = found
                }
                res.status(200).json({ success: true })
            } else {
                res.status(401).json({ success: false })
            }
        } catch (e) {
            logger.error(e.message)
            res
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .json({ message: "Fail to login" })
        }
    }

    post = async (req: express.Request, res: express.Response) => {
        //need to check password check username duplicate??
        try {
            const hash = await hashPassword(req.body.password)
            const { username, name, telephone } = req.body
            const checkDuplicate = await this.userService.getUser(username)
            if (checkDuplicate.length == 0) {
                await this.userService.createUser(username, hash, name, telephone)
                res.status(HttpStatus.OK).json({ message: "account created" })
            } else {
                res.status(HttpStatus.EXPECTATION_FAILED).json({ message: "username duplicate" })
            }

        } catch (e) {
            logger.error(e.message)
            res
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .json({ message: "Fail to create users" })
        }
    }

    getLogout = async (req: express.Request, res: express.Response) => {
        try {
            if (req.session) {
                //delete req.session.user;
                req.session.destroy(() => { })
            }
            res.json({ logout: "successed" })
        } catch (e) {
            logger.error(e.message)
            res
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .json({ message: "Fail logout" })
        }
    }

    getLoginGoogle = async (req: express.Request, res: express.Response) => {
        try {
            const accessToken = req.session?.grant.response.access_token;
            const googleResponse = await fetch('https://www.googleapis.com/oauth2/v2/userinfo', {
                headers: {
                    "Authorization": `Bearer ${accessToken}`
                }
            })
            const googleUser = await googleResponse.json()
            const { name, email } = googleUser
            const result = await this.userService.getUser(email)
            const found = result[0]
            if (found) {
                if (req.session) {
                    req.session.user = found
                }
            } else {
                const password = cryptoPassword(Math.random())
                await this.userService.createUser(email, password, name)
            }
            return res.redirect('/')

        } catch (e) {
            logger.error(e.message)
            res
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .json({ message: "Fail to login" })
        }
    }
}