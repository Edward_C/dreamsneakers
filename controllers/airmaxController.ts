import express, { Request, Response } from 'express';
import { AirMaxService } from '../services/airmaxServices'
import fs from 'fs'

export class AirMaxController {
    constructor(private airMaxServices: AirMaxService) { };
    attach(airMaxRoutes: express.Router) {
        airMaxRoutes.post('/createModel', this.createModel)
    }

    createModel = async (req: Request, res: Response) => {
        try {
            if (req.session && req.session.user) {
                const now = new Date();
                const today =`${now.getFullYear()}${now.getMonth()+1}${now.getDate()}${now.getHours()}${now.getMinutes()}${now.getSeconds()}`;
                fs.copyFileSync('./public/modules/NIKE-AirMax.glb', `./public/orderModules/${today}-${req.session.user.users_id}.glb`)
                const result = await this.airMaxServices.createSneaker(`${today}-${req.session.user.users_id}.glb`);
                console.log(result)
                await this.airMaxServices.createTempOrder(req.session.user.users_id,result[0])
                res.json({message : result})
            }
        } catch (e) {
            console.log(e)
            res.json({ message: "Fail to create model" })
        }
    }
}


