import express from 'express';
import {SneakerService} from '../services/sneakerService'


export class SneakerController{
    constructor(private sneakerService: SneakerService) { }
    attach(sneakerRouter: express.Router) {
        sneakerRouter.get('/',this.get)
    }
    
    get = async (req:express.Request, res:express.Response) => {
        try{
            const sneakerList = await this.sneakerService.getSneakers()
            res.json(sneakerList)
        }catch(e){
            console.log(e)
            res.json({"message":"Fail to get sneakers list"})
        }
    }
}
