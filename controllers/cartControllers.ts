import express from 'express';
import { CartService } from '../services/cartService'
import HttpStatus from "http-status-codes";
import { isLoggedInAPI } from '../guard';


export class CartController {
    constructor(private cartService: CartService) { }
    attach(cartRouter: express.Router) {
        cartRouter.get('/carttemp', isLoggedInAPI, this.getCartTemp)
        cartRouter.post('/', isLoggedInAPI, this.post)
        cartRouter.post('/carttemp', isLoggedInAPI, this.postCartTemp)
    }

    post = async (req: express.Request, res: express.Response) => {
        try {
            if (req.session && req.session.user) {
                const { users_id } = req.session.user
                const { address, paymentMethod} = req.body
                const cartTemps = await this.cartService.getCartTemp(req.session.user.users_id)
                let orders_status = "處理訂單中"
                for (let cartTemp of cartTemps) {
                    const {fk_sneakers_id , fk_customsneakers_id ,price} = cartTemp
                    await this.cartService.createOrder(address, price,paymentMethod, users_id, orders_status,fk_sneakers_id , fk_customsneakers_id)
                }
                this.cartService.deleteCartTemp(users_id)

                res.status(HttpStatus.OK).json({ message: "order created" })
            } else {
                res.status(HttpStatus.UNAUTHORIZED).json({ message: "Please login" })
            }
        } catch (e) {
            console.log(e);
            res
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .json({ message: "Fail to order" })
        }
    }

    getCartTemp = async (req: express.Request, res: express.Response) => {
        try {
            if (req.session && req.session.user) {
                const result = await this.cartService.getCartTemp(req.session.user.users_id)
                res.status(HttpStatus.OK).json(result)
                return
            }
            res.status(HttpStatus.UNAUTHORIZED).json({ message: "Pls login !!!" })
        } catch (e) {
            console.log(e.message)
            res
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .json({ message: "Fail to CartTemp" })
        }
    }

    postCartTemp = async (req: express.Request, res: express.Response) => {
        try {
            if (req.session && req.session.user) {
                const { users_id } = req.session.user
                const { sneakersId } = req.body
                await this.cartService.postCartTemp(users_id, sneakersId)
                res.status(HttpStatus.OK).json({ message: "add to Cart" })
            } else {
                res.status(HttpStatus.UNAUTHORIZED).json({ message: "Please login" })
            }
        } catch (e) {
            console.log(e);
            res
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .json({ message: "Fail to add to Cart" })
        }
    }
}
