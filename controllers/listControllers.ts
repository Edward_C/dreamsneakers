import express,{ Request, Response } from 'express';
import { ListService } from '../services/listServices';

export class ListController {
    constructor(private listService: ListService) { };
    attach(listRouter : express.Router){
        listRouter.get('/',this.get)
        listRouter.put('/status',this.update)
    }

    get = async (req: Request, res: Response) => {
        try{
            const ordersList = await this.listService.getOrder()
            res.json(ordersList)
        } catch (e){
            console.log(e)
            res.json({message:"Fail to get order list"})
        }
    };

    update = async(req: Request, res:Response) => {
        try {   
            const { orders_id , orders_status } = req.body
            const updateStatus = await this.listService.updateStatus(orders_id,orders_status)
            res.json(updateStatus);
        } catch (e){
            console.log(e)
            res.json({message: "Fail to change order status"})
        }
    }
}







