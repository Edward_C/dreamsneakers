import Knex from "knex";

export class SneakerService {
    constructor(public knex: Knex) { }

    async getSneakers(){
        return await this.knex("sneakers").select('*')
    }
    
}