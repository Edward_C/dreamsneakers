import Knex from "knex";

export class UserService {
    constructor(public knex: Knex) { }

    async getUser(username: string) {
        return this.knex('users').select('*').where('username',username)
    }

    async createUser(username: string, hash: string, name: string, telephone?: string) {
        return this.knex.insert({
            username: username,
            password: hash,
            name: name,
            role: 'customers',
            telephone: telephone
        }).into('users').returning('users_id')
    }
}