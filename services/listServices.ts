import Knex from 'knex';

export class ListService{
    constructor(public knex:Knex){}

    async getOrder(){
        return this.knex("orders").select('*')
                                  .join('users' ,'orders.fk_users_id','=','users.users_id')
                                  .join('sneakers' ,'orders.fk_sneakers_id','=','sneakers.sneakers_id')
    }

    async updateStatus(orders_id:number, body:any){
        const { orders_status } = body;
        return await this.knex('orders').update({
            orders_status
        }).where('orders_id',orders_id);
    }
};
