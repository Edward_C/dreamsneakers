import Knex from "knex";

export class CartService {
    constructor(public knex: Knex) { }

    async createOrder(address: string,amount:number, paymentMethod: string, userId: number, orders_status: string, fk_sneakers_id: number, fk_customsneakers_id: number) {
        return await this.knex.insert({
            quantity:1,
            deliver_place: address,
            amount:amount,
            payment_method: paymentMethod,
            fk_users_id: userId,
            orders_status: orders_status,
            fk_sneakers_id: fk_sneakers_id,
            fk_customsneakers_id: fk_customsneakers_id
        }).into('orders').returning('orders_id')
    }

    async getCartTemp(userId: number) {
        const sneakers = await this.knex('cart_temp').join('sneakers', 'sneakers.sneakers_id', '=', 'cart_temp.fk_sneakers_id').select('*').where('cart_temp.fk_users_id', userId)
        const customSneakers = await this.knex('cart_temp').join('customsneakers', 'customsneakers.customsneakers_id', '=', 'cart_temp.fk_customsneakers_id').select('*', 'sneaker_name as sneakers_name').where('cart_temp.fk_users_id', userId)

        return sneakers.concat(customSneakers)
    }

    async postCartTemp(userId: number, sneakersId: number) {
        return await this.knex.insert({
            fk_users_id: userId,
            fk_sneakers_id: sneakersId
        }).into('cart_temp')
    }

    async deleteCartTemp(userId: number) {
        await this.knex('cart_temp').where('fk_users_id',userId).del()
        return true
    }
}