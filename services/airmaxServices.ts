import Knex from "knex";

export class AirMaxService {
    constructor(public knex: Knex) { }

    async createSneaker(model: string){
        return await this.knex("customsneakers")
            .returning('customSneakers_id')
            .insert({
                sneaker_name :"airmax",
                model : model,
                price :2000
            })
    }
    async createTempOrder (fk_users_id: number, fk_customSneakers_id: number){
        return await this.knex.insert({
            fk_users_id: fk_users_id,
            fk_customSneakers_id: fk_customSneakers_id
        }).into("cart_temp")
    }
}