import sha256 from 'crypto-js/sha256';
import hmacSHA512 from 'crypto-js/hmac-sha512';
import Base64 from 'crypto-js/enc-base64';


export function cryptoPassword (message:number) {
    const nonce = "A", path = "A", privateKey = "A"; // ...
    const hashDigest = sha256(nonce + message);
    const hmacDigest = Base64.stringify(hmacSHA512(path + hashDigest, privateKey));
    return hmacDigest
}