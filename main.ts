import express from 'express';
import expressSession from 'express-session';
import exphbs from 'express-handlebars';
import bodyParser from 'body-parser';
import path from 'path'
import Knex from 'knex';
import grant from 'grant-express';
import { ListService } from './services/listServices'
import { ListController } from './controllers/listControllers'
import { UserService } from './services/UserService';
import { UserController } from './controllers/UserController';
import { CartService } from './services/cartService';
import { CartController } from './controllers/cartControllers';
import { SneakerService } from './services/sneakerService';
import { SneakerController } from './controllers/sneakerControllers';
import { AirMaxService } from './services/airmaxServices';
import { AirMaxController } from './controllers/airmaxController'
import { logger } from './logger';


const app = express();
const knexConfig = require('./knexfile');
const knex = Knex(knexConfig["development"])



app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(expressSession({
    secret: 'Tecky Academy teaches typescript',
    resave: true,
    saveUninitialized: true
}));

//google login
app.use(grant({
    "defaults": {

        "protocol": process.env.PROTOCOL || "http",
        "host": process.env.HOST || "localhost:8080",
        "transport": "session",
        "state": true,
    },
    "google": {
        "key": process.env.GOOGLE_CLIENT_ID || "",
        "secret": process.env.GOOGLE_CLIENT_SECRET || "",
        "scope": ["profile", "email"],
        "callback": "/users/login/google"
    },
}));

//express-handlebars
app.engine('handlebars', exphbs());
app.set('views', './public')
app.set('view engine', 'handlebars');

app.get('/', function (req: express.Request, res: express.Response) {
    res.render('index', { layout: 'header' });
});
app.get('/question', function (req: express.Request, res: express.Response) {
    res.render('question', { layout: 'header' });
});
app.get('/cart', (req, res) => {
    res.render('cart', { layout: 'header' });
})
app.get('/product_category', (req, res) => {
    res.render('product_category', { layout: 'header' })
})
app.get('/order_list', (req, res) => {
    res.render('list', { layout: 'header' });
})
app.get('/airMax', (req, res) => {
    // res.render('airMax', {layout : 'header'});
    res.sendFile(path.join(__dirname, 'public', 'airMax.html'))
})
app.get('/customList', (req, res) => {
    res.render('customList', { layout: 'header' });
})


app.get('/Nike_AirMax_model', (req, res) => {
    res.sendFile(path.join(__dirname, 'public', '/modules/NIKE-AirMax.glb'))
})

app.get('/test', (req, res) => {
    res.sendFile(path.join(__dirname, 'public', '/orderModules/20208219193-4.glb'))
})


// list MVC //
const listRoutes = express.Router()
const listService = new ListService(knex)
export const listController = new ListController(listService);
listController.attach(listRoutes);
app.use('/list', listRoutes);

//user mvc
const userRoutes = express.Router()
const userService = new UserService(knex)
const userController = new UserController(userService)
userController.attach(userRoutes)
app.use('/users', userRoutes)

//cart mvc
const cartRoutes = express.Router()
const cartService = new CartService(knex)
const cartController = new CartController(cartService)
cartController.attach(cartRoutes)
app.use('/cart', cartRoutes)

//sneaker mvc
const sneakerRoutes = express.Router()
const sneakerService = new SneakerService(knex)
const sneakerController = new SneakerController(sneakerService)
sneakerController.attach(sneakerRoutes)
app.use('/sneaker', sneakerRoutes)

// airMax mvc 
const airMaxRoutes = express.Router()
const airMaxService = new AirMaxService(knex)
const airMaxController = new AirMaxController(airMaxService)
airMaxController.attach(airMaxRoutes);
app.use('/airmax', airMaxRoutes)




app.use(express.static('public'));

const PORT = 8080;
app.listen(PORT, () => {
    logger.info(`Server listing on http://localhost:${PORT}`);
})