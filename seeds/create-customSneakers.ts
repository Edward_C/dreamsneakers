import * as Knex from "knex";

const custom = "customsneakers"

export async function seed(knex: Knex): Promise<void> {
    // Inserts seed entries
    await knex(custom).del()

    await knex(custom).insert([
        {sneaker_name: 'nike_ariMax',model : 'NIKE-AirMax.glb', price : 2000 }
    ]);
};
