import * as Knex from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    // await knex("sneakers").del()

    // Inserts seed entries
    // await knex("users").insert([
    //     { username: "guest1", password: "guest1", role: "admin", name: "guest1" },
    //     { username: "admin1", password: "admin1", role: "admin", name: "admin1" }
    // ]);

    await knex("sneakers").insert([
        { image: '01hill.jpg', ranking: "5", sneakers_name: "Nike Air Max 1", price:4000},
        { image: '02hill.jpg', ranking: "5", sneakers_name: "Nike Air Presto", price:3000},
        { image: '03hill.jpg', ranking: "5", sneakers_name: "Nike Blazer Low", price:5000},
        { image: '04hill.jpg', ranking: "5", sneakers_name: "Nike Air Max 270 React", price:6000},
        { image: '05football.jpg', ranking: "5", sneakers_name: "Nike Mercurial Superfly 7 Academy", price:6000},
        { image: '06football.jpg', ranking: "5", sneakers_name: "Nike Vapor Edge Pro 360", price:5000},
        { image: '07football.jpg', ranking: "5", sneakers_name: "Nike Phantom Vision Elite", price:4000},
        { image: '08football.jpg', ranking: "5", sneakers_name: "Nike Phantom GT Elite", price:3000},
        { image: '09basketball.jpg', ranking: "5", sneakers_name: "Kyrie 6", price:2000},
        { image: '10basketball.jpg', ranking: "5", sneakers_name: "KD 13", price:3000},
        { image: '11basketball.jpg', ranking: "5", sneakers_name: "Nike Zoom Freak 1", price:4000},
        { image: '12basketball.jpg', ranking: "5", sneakers_name: "PG 4", price:5000},
        { image: '13run.jpg', ranking: "5", sneakers_name: "Nike Free Rn 3.0", price:7000},
        { image: '14run.jpg', ranking: "5", sneakers_name: "Nike Free Rn 5.0", price:9000},
        { image: '15run.jpg', ranking: "5", sneakers_name: "Nike React Infinity Run Flyknit", price:8000},
        { image: '16run.jpg', ranking: "5", sneakers_name: "Nike Air Max 90 Unlocked", price:10000}
    ])

    // await knex("orders").insert([
    //     { quantity: 1, deliver_place: "Home", amount: 1000, payment_method: "Cash", orders_status: "製作中", fk_users_id: 1, fk_sneakers_id: 1 }
    // ])

    // await knex("recommendations").insert([
    //     { image: "rec_1.jpg", tag: "AJ", fk_users_id: 1, fk_sneakers_id: 1 }
    // ])
};
        
