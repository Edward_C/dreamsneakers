async function loadCartTemp() {
  const orderList = document.querySelector('#order-list')
  res = await fetch('/cart/carttemp')
  const orders = await res.json()
  orderList.innerHTML = ``
  let i = 1
  
  for (order of orders) {
    orderList.innerHTML += `
      <tr>
        <th scope="row">${i}</th>
        <td>${order.sneakers_name}</td>
        <td>1</td>
        <td>$${order.price}</td>
      </tr>
    `
    i++
  }
}
loadCartTemp()

function createOrder() {
  document.querySelector('#cart-from')
    .addEventListener('submit', async function (event) {
      event.preventDefault()

      // const res = await fetch('/cart/carttemp')
      // const carttemp = await res.json();

      const form = event.target
      const formObj = {}
      formObj.address = form.address.value
      formObj.paymentMethod = form.paymentMethod.value


      const res = await fetch('/cart', {
        method: 'POST',
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(formObj)
      })

      const result = await res.json()
      
      if (res.status === 200){
        document.querySelector('#submit－cart-btn').innerHTML = '已提交'
      }else (
        console.log(res.status)
      )
    })
}
createOrder()
