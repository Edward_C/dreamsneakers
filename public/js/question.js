
function recommendation() {
  const questionForm = document.querySelector("#question-from")
  let submitButton = document.querySelector('#submit')
  questionForm.addEventListener('submit', async function (event) {
    event.preventDefault()
    const form = event.target
    printSneakers(form.q1.value)
  })

}
recommendation()

async function printSneakers(answer) {
  let rec;
  switch (answer) {
    case "1": {
      rec = "hill"
      console.log("1")
      break;
    }
    case "2":{
      rec = "football"
      break;
    }
    case "3":{
      rec = "basketball"
      break;
    }
    case "4":{
      rec = "run"
      break;
    }
  }
  console.log(answer)
  const res = await fetch('/sneaker');
  const sneakers = await res.json();
  const recommendSneakers = sneakers.filter(sneakers => sneakers.image.includes(rec))

  const sneakersContainer = document.querySelector('#recommend')
  sneakersContainer.innerHTML = ''
  for (let sneaker of recommendSneakers) {
    sneakersContainer.innerHTML += `
    <div class="col-4 mb-4">
    <div class="card h-100">
      <img src="./image/sneakers/${sneaker.image}" class="card-img-top" alt="...">
      <div class="card-body">
        <h5 class="card-title">${sneaker.sneakers_name}</h5>
        <p class="card-text">$${sneaker.price}</p>
        <button type="submit" class="btn btn-primary addToCart-btn" data-id="${sneaker.sneakers_id}">加入購物車</button>
      </div>
    </div>
  </div>`
  }
  setAddToCartBtns()
}


function setAddToCartBtns() {
  const addToCartBtns = Array.from(document.querySelectorAll('.addToCart-btn'))
  for (const addToCartBtn of addToCartBtns) {
    addToCartBtn.onclick = async function () {
      console.log('setAddToCartBtns2')
      sneakersId = addToCartBtn.getAttribute('data-id')
      const res = await fetch(`/cart/carttemp`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({ sneakersId: sneakersId })
      })
      if(res.status === 401){
        alert("Please login first!");
        return;
      }
      addToCartBtn.innerHTML = '已加入'
      addToCartBtn.classList.remove("btn-primary")
      addToCartBtn.classList.add("btn-warning")
      const result = await res.json();
    }
  }
}



