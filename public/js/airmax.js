const canvas = document.querySelector('#model')
const bubble = document.querySelector("#bubble")
const eyelets = document.querySelector("#eyelets")
const frontUpper = document.querySelector('#frontUpper')
const healGuard = document.querySelector('#healGuard')
const healUpper = document.querySelector('#healUpper')
const liner = document.querySelector('#liner')
const mainLeft = document.querySelector('#mainLeft')
const mainRight = document.querySelector('#mainRight')
const toeGuard = document.querySelector('#toeGuard')
const trim = document.querySelector('#trim')
const trim_front = document.querySelector('#trim_front')
const laces = document.querySelector('#laces')
const sole = document.querySelector('#sole')
const soleFoam = document.querySelector('#soleFoam')
const stitching = document.querySelector('#stitching')
const swooshLeft = document.querySelector('#swooshLeft')
const swooshRight = document.querySelector('#swooshRight')
const tongueFront = document.querySelector('#tongueFront')
const tongueLabel = document.querySelector('#tongueLabel')
const tongueTop = document.querySelector('#tongueTop');


const scene = new THREE.Scene();
var theModel;
const MODEL_PATH = "/Nike_AirMax_model";

const renderer = new THREE.WebGLRenderer({ canvas, antialias: true })
renderer.setSize(700, 700)
renderer.setClearColor(0xAEAEAE, 1.0)

var loader = new THREE.GLTFLoader();
const imageLoader = new THREE.TextureLoader();

const INITIAL_MTL = new THREE.MeshPhongMaterial();

const INITIAL_MAP = [
    { childID: "air_bubble_left", mtl: INITIAL_MTL },
    { childID: "air_bubble_right", mtl: INITIAL_MTL },
    { childID: "eyelets_left", mtl: INITIAL_MTL },
    { childID: "eyelets_right", mtl: INITIAL_MTL },
    { childID: "fabric_front_upper", mtl: INITIAL_MTL },
    { childID: "fabric_heal_guard", mtl: INITIAL_MTL },
    { childID: "fabric_heal_upper", mtl: INITIAL_MTL },
    { childID: "fabric_liner", mtl: INITIAL_MTL },
    { childID: "fabric_main_left", mtl: INITIAL_MTL },
    { childID: "fabric_main_right", mtl: INITIAL_MTL },
    { childID: "fabric_toe_guard", mtl: INITIAL_MTL },
    { childID: "fabric_trim", mtl: INITIAL_MTL },
    { childID: "fabric_trim_front_left", mtl: INITIAL_MTL },
    { childID: "fabric_trim_front_right", mtl: INITIAL_MTL },
    { childID: "fabric_upper_mid", mtl: INITIAL_MTL },
    { childID: "fabric_upper_mid2_right", mtl: INITIAL_MTL },
    { childID: "laces", mtl: INITIAL_MTL },
    { childID: "sole", mtl: INITIAL_MTL },
    { childID: "sole_foam", mtl: INITIAL_MTL },
    { childID: "stitching", mtl: INITIAL_MTL },
    { childID: "swoosh_left", mtl: INITIAL_MTL },
    { childID: "swoosh_right", mtl: INITIAL_MTL },
    { childID: "tongue_front", mtl: INITIAL_MTL },
    { childID: "tongue_label", mtl: INITIAL_MTL },
    { childID: "tongue_top", mtl: INITIAL_MTL }
];


loader.load(MODEL_PATH, function (gltf) {
    theModel = gltf.scene;

    theModel.traverse((o) => {
        if (o.isMesh) {
            o.castShadow = true;
            o.receiveShadow = true;
        }
    });

    // Set the models initial scale   
    theModel.scale.set(20, 20, 20);

    // Add the model to the scene
    theModel.position.x = 1;
    theModel.position.y = -150;

    function initColor(parent, type, mtl) {
        parent.traverse((o) => {
            if (o.isMesh) {
                if (o.name.includes(type)) {
                    o.material = mtl;
                    o.nameID = type;
                }
            }
        });
    }

    for (let object of INITIAL_MAP) {
        initColor(theModel, object.childID, object.mtl);
    }

    scene.add(theModel);


}, undefined, function (error) {
    console.error(error)
});

// document.body.appendChild(renderer.domElement)
document.querySelector("#model-location").appendChild(renderer.domElement)

camera = new THREE.PerspectiveCamera(
    100,
    window.innerWidth / window.innerHeight,
    100,
    10000
)
camera.position.set(350, 300, 000)
camera.lookAt(scene.position)

const cameraControl = new THREE.OrbitControls(camera, renderer.domElement)
cameraControl.enableDamping = true
cameraControl.dampingFactor = 0.25
cameraControl.enablePan = false

let pointLight1 = new THREE.DirectionalLight(0xDDDDDD,0.9)
pointLight1.position.set(1000, 1000, 1000)
scene.add(pointLight1)

const ambientLight = new THREE.AmbientLight(0xCBCBCB,1)
scene.add(ambientLight)

// Change Color (Start) //
bubble.addEventListener('click',Bubble);
eyelets.addEventListener('click',Eyelets);
frontUpper.addEventListener('click',FrontUpper);
healGuard.addEventListener('click',HealGuard);
healUpper.addEventListener('click',HealUpper);
liner.addEventListener('click',Liner);
mainLeft.addEventListener('click',MainLeft);
mainRight.addEventListener('click',MainRight);
toeGuard.addEventListener('click',toeGuardColor);
trim.addEventListener('click',Trim);
trim_front.addEventListener('click',TrimFront);
laces.addEventListener('click',Laces);
sole.addEventListener('click',Sole);
soleFoam.addEventListener('click',SoleFoam);
stitching.addEventListener('click',Stitching);
swooshLeft.addEventListener('click',SwooshLeft);
swooshRight.addEventListener('click',SwooshRight);
tongueFront.addEventListener('click',TongueFront);
tongueLabel.addEventListener('click',TongueLabel);
tongueTop.addEventListener('click',TongueTop);


function Bubble(e){
    let color = new THREE.Color(bubble.value)
    let new_mtl
  
    new_mtl = new THREE.MeshPhongMaterial({
      color: color,
      shininess: color.shininess ? color.shininess : 10,
    });
  
    setMaterial(theModel, 'air_bubble_left', new_mtl);
    setMaterial(theModel, 'air_bubble_right', new_mtl);
}

function Eyelets(e){
    let color = new THREE.Color(eyelets.value)
    let new_mtl
  
    new_mtl = new THREE.MeshPhongMaterial({
      color: color,
      shininess: color.shininess ? color.shininess : 10,
    });
  
    setMaterial(theModel, 'eyelets_left', new_mtl);
    setMaterial(theModel, 'eyelets_right', new_mtl);
}

function FrontUpper(e){
    let color = new THREE.Color(frontUpper.value)
    let new_mtl
  
    new_mtl = new THREE.MeshPhongMaterial({
      color: color,
      shininess: color.shininess ? color.shininess : 10,
    });
  
    setMaterial(theModel, 'fabric_front_upper', new_mtl);
    setMaterial(theModel, 'fabric_upper_mid', new_mtl);
    setMaterial(theModel, 'fabric_upper_mid2_right', new_mtl);
}

function HealGuard(e){
    let color = new THREE.Color(healGuard.value)
    let new_mtl
  
    new_mtl = new THREE.MeshPhongMaterial({
      color: color,
      shininess: color.shininess ? color.shininess : 10,
    });
  
    setMaterial(theModel, 'fabric_heal_guard', new_mtl);
}

function HealUpper(e){
    let color = new THREE.Color(healUpper.value)
    let new_mtl
  
    new_mtl = new THREE.MeshPhongMaterial({
      color: color,
      shininess: color.shininess ? color.shininess : 10,
    });
  
    setMaterial(theModel, 'fabric_heal_upper', new_mtl);
}

function Liner(e){
    let color = new THREE.Color(liner.value)
    let new_mtl
  
    new_mtl = new THREE.MeshPhongMaterial({
      color: color,
      shininess: color.shininess ? color.shininess : 10,
    });
  
    setMaterial(theModel, 'fabric_liner', new_mtl);
}

function MainLeft(e){
    let color = new THREE.Color(mainLeft.value)
    let new_mtl
  
    new_mtl = new THREE.MeshPhongMaterial({
      color: color,
      shininess: color.shininess ? color.shininess : 10,
    });
  
    setMaterial(theModel, 'fabric_main_left', new_mtl);
}

function MainRight(e){
    let color = new THREE.Color(mainRight.value)
    let new_mtl
  
    new_mtl = new THREE.MeshPhongMaterial({
      color: color,
      shininess: color.shininess ? color.shininess : 10,
    });
  
    setMaterial(theModel, 'fabric_main_right', new_mtl);
}

function toeGuardColor(e){
    let color = new THREE.Color(toeGuard.value)
    let new_mtl
  
    new_mtl = new THREE.MeshPhongMaterial({
      color: color,
      shininess: color.shininess ? color.shininess : 10,
    });
  
    setMaterial(theModel, 'fabric_toe_guard', new_mtl);
}

function Trim(e){
    let color = new THREE.Color(trim.value)
    let new_mtl
  
    new_mtl = new THREE.MeshPhongMaterial({
      color: color,
      shininess: color.shininess ? color.shininess : 10,
    });
  
    setMaterial(theModel, 'fabric_trim', new_mtl);
}

function TrimFront(e){
    let color = new THREE.Color(trim_front.value)
    let new_mtl
  
    new_mtl = new THREE.MeshPhongMaterial({
      color: color,
      shininess: color.shininess ? color.shininess : 10,
    });

    setMaterial(theModel, 'fabric_trim_front_left', new_mtl);
    setMaterial(theModel, 'fabric_trim_front_right', new_mtl);
}

function Laces(e){
    let color = new THREE.Color(laces.value)
    let new_mtl
  
    new_mtl = new THREE.MeshPhongMaterial({
      color: color,
      shininess: color.shininess ? color.shininess : 10,
    });

    setMaterial(theModel, 'laces', new_mtl);
}

function Sole(e){
    let color = new THREE.Color(sole.value)
    let new_mtl
  
    new_mtl = new THREE.MeshPhongMaterial({
      color: color,
      shininess: color.shininess ? color.shininess : 10,
    });

    setMaterial(theModel, 'sole', new_mtl);
}

function SoleFoam(e){
    let color = new THREE.Color(soleFoam.value)
    let new_mtl
  
    new_mtl = new THREE.MeshPhongMaterial({
      color: color,
      shininess: color.shininess ? color.shininess : 10,
    });

    setMaterial(theModel, 'sole_foam', new_mtl);
}

function Stitching(e){
    let color = new THREE.Color(stitching.value)
    let new_mtl
  
    new_mtl = new THREE.MeshPhongMaterial({
      color: color,
      shininess: color.shininess ? color.shininess : 10,
    });

    setMaterial(theModel, 'stitching', new_mtl);
}

function SwooshLeft(e){
    let color = new THREE.Color(swooshLeft.value)
    let new_mtl
  
    new_mtl = new THREE.MeshPhongMaterial({
      color: color,
      shininess: color.shininess ? color.shininess : 10,
    });

    setMaterial(theModel, 'swoosh_left', new_mtl);
}

function SwooshRight(e){
    let color = new THREE.Color(swooshRight.value)
    let new_mtl
  
    new_mtl = new THREE.MeshPhongMaterial({
      color: color,
      shininess: color.shininess ? color.shininess : 10,
    });

    setMaterial(theModel, 'swoosh_right', new_mtl);
}

function TongueFront(e){
    let color = new THREE.Color(tongueFront.value)
    let new_mtl
  
    new_mtl = new THREE.MeshPhongMaterial({
      color: color,
      shininess: color.shininess ? color.shininess : 10,
    });

    setMaterial(theModel, 'tongue_front', new_mtl);
}

function TongueLabel(e){
    let color = new THREE.Color(tongueLabel.value)
    let new_mtl
  
    new_mtl = new THREE.MeshPhongMaterial({
      color: color,
      shininess: color.shininess ? color.shininess : 10,
    });

    setMaterial(theModel, 'tongue_label', new_mtl);
}

function TongueTop(e){
    let color = new THREE.Color(tongueTop.value)
    let new_mtl
  
    new_mtl = new THREE.MeshPhongMaterial({
      color: color,
      shininess: color.shininess ? color.shininess : 10,
    });

    setMaterial(theModel, 'tongue_top', new_mtl);
}

function setMaterial(parent, type, mtl) {
    parent.traverse((o) => {
        if (o.isMesh && o.nameID != null) {
            if (o.nameID == type) {
                o.material = mtl;
            }
        }
    });
}
// Change Color (End) //

function render() {
    requestAnimationFrame(render)
    cameraControl.update();
    renderer.render(scene, camera)
}

function createModel(){
  const create = document.querySelector('#next');
  const model = document.querySelector('#model')

  create.onclick = async function(){
    const res = await fetch(`/airmax/createModel`,{
      method : "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({model:model})
    })
  }
}
createModel();
render();