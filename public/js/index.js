function fetchHeader() {
    // const res = await fetch('/header.html')
    // const html = await res.text()
    // document.querySelector('#header').innerHTML = html
    createUser()
    login()
    logout()
    checkPasswordConfirmation()
    checkPhoneNumber()
}
fetchHeader()

function createUser() {
    const signUpFrom = document.querySelector('#sign-up-from')
    const innerSignUpFrom = document.querySelector('#inner-sign-up-from')
    document.querySelector('#sign-up-btn').onclick = function () {
        innerSignUpFrom.innerHTML = `
            <input type="text" name="username" placeholder="Username" required>
            <div class='d-none warning' id="username-duplicate">Username duplicate </div>
            <input type="password" name="password" placeholder="Password" id="password"
                required>
            <input type="password" name="password-confirmation"
            placeholder="Password confirmation" id="password-confirmation" required>
            <div class="d-none warning" id="pw-confirm-warn">password confirmation is not same as password</div>
            <input type="text" name="name" placeholder="Name" required>
            <input type="number" name="telephone" placeholder="Telephone" id="telephone" required>
            <div class='d-none warning' id="check-phone">The phone number is invalid.</div>
            <input type="submit" value="Sign up" id="submit-btn">
            `
        checkPasswordConfirmation()
        checkPhoneNumber()
    }

    signUpFrom.addEventListener('submit', async function (event) {
        event.preventDefault()
        const form = event.target
        const formObj = {}
        formObj.username = form.username.value
        formObj.password = form.password.value
        formObj.name = form.name.value
        formObj.telephone = form.telephone.value

        const res = await fetch('/users', {
            method: 'POST',
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(formObj)
        })

        const result = await res.json()
        console.log(res.status)

        if (res.status === 200) {
            innerSignUpFrom.innerHTML = `<h3 class='sign-up-message'> You have signed up <br>
                                        successfully </h3>`
        } else if(res.status === 417){
            document.querySelector('#username-duplicate').classList.remove('d-none')
        } else{console.log(result)}
    })
    document.querySelector('#submit-btn')
}

function checkPasswordConfirmation() {
    const password = document.querySelector('#password')
    const passwordConfirmation = document.querySelector('#password-confirmation')
    const submitBtn = document.querySelector('#submit-btn')
    const pwConfirmWarn = document.querySelector('#pw-confirm-warn')
    pwConfirmWarn.classList.add('d-none')
    password.onkeyup = onkeyup
    passwordConfirmation.onkeyup = onkeyup

    function onkeyup() {
        if (password.value !== passwordConfirmation.value) {
            pwConfirmWarn.classList.remove('d-none')
            submitBtn.disabled = true;
        } else {
            pwConfirmWarn.classList.add('d-none')
            submitBtn.disabled = false;
        }
    }
}

function checkPhoneNumber() {
    const telephone = document.querySelector('#telephone')
    const checkPhone = document.querySelector('#check-phone')
    const submitBtn = document.querySelector('#submit-btn')
    telephone.onkeyup = onkeyup

    function onkeyup() {
        if (telephone.value.length !== 8) {
            checkPhone.classList.remove('d-none')
            submitBtn.disabled = true;
        } else {
            checkPhone.classList.add('d-none')
            submitBtn.disabled = false;
        }
    }
}


function login() {
    document.querySelector('#login-from')
        .addEventListener('submit', async function (event) {
            event.preventDefault()
            const form = event.target
            const formObj = {}
            formObj.username = form.username.value
            formObj.password = form.password.value

            const res = await fetch('/users/login', {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(formObj)
            })
            if (res.status === 200) {
                window.location.replace(window.location.pathname);
            } else {
                document.querySelector('#login-fail').classList.remove('d-none')
            }
        })
    checkLogin ()
}

async function checkLogin () {
    const res = await fetch('/users')
    const user = await res.json()
    if(res.status === 200 && user.username){
        document.querySelector('#login-btn').classList.add('d-none')
        document.querySelector('#sign-up-btn').classList.add('d-none')
        document.querySelector('#username-btn').classList.remove('d-none')
        document.querySelector('#logout-btn').classList.remove('d-none')
        document.querySelector('#username-btn').innerHTML = `${user.username}`
    }else{
        document.querySelector('#login-btn').classList.remove('d-none')
        document.querySelector('#sign-up-btn').classList.remove('d-none')
    }
}

async function logout() {
    const logoutBtn = document.querySelector('#logout-btn')
    logoutBtn.onclick = async function () {
        await fetch('/users/logout')
        window.location.replace(window.location.pathname)
    }
    
}
