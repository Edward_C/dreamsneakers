const orderData = document.querySelector('#orderlist');


async function checkOrder() {
    const res0 = await fetch('/list');
    const lists = await res0.json();
    const res1 = await fetch('/users')
    const users = await res1.json();
    // console.log(lists);
    console.log(users);

    // const orderData = document.querySelector('#orderlist');
    orderData.innerHTML = ''
    if (users.role === "customers") {
        for (let list of lists) {
            if (list.fk_users_id === users.users_id)
                orderData.innerHTML += `
                    <div class="row data_bar">
                        <div class="col-6 col-md invoice_no order_data">${list.orders_id}</div>
                        <div class="col-6 col-md mage  order_data">${list.sneakers_name}</div>
                        <div class="col-6 col-md quantity  order_data">${list.quantity}件</div>
                        <div class="col-6 col-md address  order_data">${list.deliver_place}</div>
                        <div class="col-6 col-md content  order_data">${list.telephone}</div>
                        <div class="col-6 col-md amount  order_data">$${list.amount}</div>
                        <div class="col-6 col-md payment  order_data">${list.payment_method}</div>
                        <div class="col-6 col-md status  order_data">${list.orders_status}</div>
                    </div>
                `
        }
    } else if (users.role === "admin") {
        for (let list of lists) {
            orderData.innerHTML += `
                    <div class="row data_bar">
                        <div class="col invoice_no order_data">${list.orders_id}</div>
                        <div class="col image  order_data">${list.sneakers_name}</div>
                        <div class="col quantity  order_data">${list.quantity}件</div>
                        <div class="col address  order_data">${list.deliver_place}</div>
                        <div class="col content  order_data">${list.telephone}</div>
                        <div class="col amount  order_data">$${list.amount}</div>
                        <div class="col payment  order_data">${list.payment_method}</div>
                        <div class="col status  order_data">
                            <from action="/status" method="PUT" id="status-from">
                                <div class="row">
                                    <div class="col">
                                        <select name="status">
                                            <option value="處埋訂單中" name="處埋訂單中">處埋訂單中</option>
                                            <option value="製作中" name="製作中">製作中</option>
                                            <option value="運送中" name="運送中">運送中</option>
                                            <option value="已完成" name="已完成">已完成</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <input type="submit" value="確定">
                                        <input type="submit" value="取消">
                                    </div>
                                </div>
                            </from>
                        </div>
                    </div>

                `
        }
    }
}


// Search Bar (Start) //
function myFunction(){
    let input = document.querySelector('#searchBar');
    let filter = input.value.toUpperCase();
    let ul = document.querySelector("#orderlist");
    let span = ul.getElementsByTagName("span");
    
    for(i = 0; i < span.length; i++){
        a = span[i].getElementsByTagName("a")[0];
        textValue = a.textContent || a.innerText;
        
        if(textValue.toUpperCase().indexOf(filter) > -1){
            ul.style.display = "";
        } else {
            span[i].style.display = "none";
        }
    }
}
// Search Bar (End) //

checkOrder();
myFunction();