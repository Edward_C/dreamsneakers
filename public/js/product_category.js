async function printSneakers() {
  const res = await fetch('/sneaker');
  const sneakers = await res.json();
  // console.log(sneakers)

  const sneakersContainer = document.querySelector('#sneakers-container')
  sneakersContainer.innerHTML = ''
  for (let sneaker of sneakers) {
    sneakersContainer.innerHTML += `
    <div class="col mb-4">
    <div class="card h-100">
      <img src="./image/sneakers/${sneaker.image}" class="card-img-top" alt="...">
      <div class="card-body">
        <h5 class="card-title">${sneaker.sneakers_name}</h5>
        <p class="card-text">$${sneaker.price}</p>
        <button type="submit" class="btn btn-primary addToCart-btn" data-id="${sneaker.sneakers_id}">加入購物車</button>
      </div>
    </div>
  </div>`
  }
  setAddToCartBtns()
}
printSneakers()


function setAddToCartBtns() {
  const addToCartBtns = Array.from(document.querySelectorAll('.addToCart-btn'))
  for (const addToCartBtn of addToCartBtns) {
    addToCartBtn.onclick = async function () {
      console.log('setAddToCartBtns2')
      sneakersId = addToCartBtn.getAttribute('data-id')
      const res = await fetch(`/cart/carttemp`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({ sneakersId: sneakersId })
      })
      if(res.status === 401){
        alert("Please login first!");
        return;
      }
      addToCartBtn.innerHTML = '已加入'
      addToCartBtn.classList.remove("btn-primary")
      addToCartBtn.classList.add("btn-warning")
      const result = await res.json();
    }
  }
}